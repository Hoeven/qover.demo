import React from 'react';
import './style/style.css'
import LoginScreen from './screens/login/LoginScreen'
import QuoteScreen from './screens/quote/QuoteScreen'
import SelectPlanScreen from './screens/plans/SelectPlanScreen'
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { Colors } from './constants'
import { StylesProvider } from '@material-ui/core/styles';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: Colors.darkSkyBlue,
    },
    secondary: {
      main: Colors.aquaMarine
    }
  },
  typography: {
    htmlFontSize: 10,
    button: {
      fontSize: '1.4rem',
      textTransform: 'none',
      padding: '2rem',
      fontWeight: 500
    },
  },
  overrides: {
    MuiOutlinedInput: {
      root: {
        borderRadius: '2px'
      }
    },
    MuiButton: {
      root: { 
        padding: '1rem',
        borderColor: Colors.white
      },
      outlined: {
        padding: '1rem',
      },
    },
  },

})


function App() {
  return (

    <StylesProvider injectFirst>
      <ThemeProvider theme={theme}>
        <Router>
            <Switch>
              <Route path="/login">
                <LoginScreen />
              </Route>
              <Route path="/quote">
                <QuoteScreen />
              </Route>
              <Route path="/plan">
                <SelectPlanScreen />
              </Route>
              <Route path="/">
                <LoginScreen />
              </Route>
            </Switch>
        </Router>
      </ThemeProvider>
    </StylesProvider>
  );
}

export default App;
