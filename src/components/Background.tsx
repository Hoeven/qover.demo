import React, { FunctionComponent } from 'react'
import styled from 'styled-components'

import colors from '../constants/Colors'

type Props = {
    children: React.ReactNode
}

const Background: FunctionComponent<Props> = ({ children }) => (
    <FullScreen>
        <Gradient>
            <Centered>
                {children}
            </Centered>
        </Gradient>
    </FullScreen>
)


const FullScreen = styled.div`
  height: 100vh;
  width: 100vw;
`;

const Gradient = styled.div`
  background: linear-gradient(
    150deg,
    ${colors.darkSkyBlue},
    ${colors.teal}  
  );
  height: 100%;
  width: 100%;
`;

const Centered = styled.div`
  display: flex;
  justify-content: center;
  align-items:center;
  flex-direction: column;
  height: 100vh;
`;  

export default Background