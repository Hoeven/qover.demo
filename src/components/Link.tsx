import React, { FunctionComponent } from 'react'
import { Link as MaterialUILink } from '@material-ui/core';
import styled from 'styled-components'


type Props = {
    onClick: (event: React.MouseEvent<HTMLAnchorElement>) => void
    text:string,
    href: string,
}

const Link: FunctionComponent<Props> = ({ onClick, text, href }) => (
    <StyledLink underline='none' href={href} onClick={onClick}>{text}</StyledLink>)


const StyledLink = styled(MaterialUILink)`
    font-weight: 600;
    underline: none;
    font-size: 1.2rem;
`
export default Link