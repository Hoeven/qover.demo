export default { 
    aquaMarine: '#31cfda',
    blueGrey: '#5b7289',
    cloudyBlue: '#d4dce2',
    darkSkyBlue: '#317bda',
    teal: '#33c3c8',
    white: '#ffffff',
    black: '#000000'
}