import React, { useState } from 'react';
import logo from '../../images/logo.svg'
import { Background, Card, Link } from '../../components'
import { Colors, Sizes } from '../../constants'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import styled from 'styled-components'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline'
import { useHistory } from "react-router-dom"
import UserService from '../../services/userService'


const useInput = (initialValue: any, validator?: any) => {
  const [value, setValue] = useState(initialValue);
  const [validationResult, setValidationResult] = useState({ error: false, message: '' })

  return {
    isValid: !validationResult.error,
    validate: () => {
      if (!validator) return true
      const result = validator(value)
      setValidationResult(result)
      return !result.error
    },
    value,
    setValue,
    reset: () => setValue(''),
    bind: {
      error: validationResult.error,
      helperText: validationResult.message,
      value,
      onChange: (event: any) => {
        setValue(event.target.value)
        setValidationResult({ error: false, message: '' })
      }
    }
  }
}

export const passwordValidator = (password: string) => {
  if (password === undefined || password.length === 0) return { error: true, message: 'password is required' }
  return password.length >= 6
    ? { error: false, message: '' }
    : { error: true, message: 'Password must be at least 6 characters long' }
}

export const emailValidator = (email: string) => {
  if (email === undefined || email.length === 0) return { error: true, message: 'email is required' }
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email)
    ? { error: false, message: '' }
    : { error: true, message: 'Must be a valid email' }
}

const LoginScreen = () => {
  const history = useHistory();
  const [error, setError] = useState(false);

  const { value: email, bind: bindEmail, validate: validateEmail } = useInput('', emailValidator);
  const { value: password, bind: bindPassword, validate: validatePassword } = useInput('', passwordValidator);

  const handleLoginClicked = async () => {
    try {
      setError(false)
      const isEmailValid = validateEmail()
      const isPasswordValid = validatePassword()
      if (isEmailValid && isPasswordValid) {
        await UserService.login(email, password)
        history.push("/quote")
      }
    }
    catch (e) {
      setError(true)
    }
  }

  return (
    <>
      <Background>
        <img src={logo} alt="logo"></img>
        <LoginFormContainer>
          <Card size={Sizes.small}>
            <Form noValidate autoComplete="off">
              <Title>Welcome at Qover</Title>
              <div style={{ marginBottom: '1rem', display: 'flex', flexDirection: 'column' }}>
                <TextField
                  id="standard-basic"
                  label="Email"
                  {...bindEmail}
                />
                <TextField
                  id="standard-password-input"
                  label="Password"
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                  {...bindPassword}
                />
              </div>
              <BottomControlsContainer>
                <FormControlLabel
                  control={<Checkbox color="primary" checkedIcon={<CheckCircleOutlineIcon />} value="checkedH" />}
                  label={<CheckBoxText>Remember me</CheckBoxText>}
                />
                <Link href='#' onClick={() => window.alert('become a qover gold member to access this feature')} text='Forgot your password?' />
              </BottomControlsContainer>
              <Button variant="contained" color="primary" onClick={handleLoginClicked}>
                Sign in to your account
              </Button>
              <FormErrorContainer>
                {error && <FormHelperText error>Invalid email or password</FormHelperText>}
              </FormErrorContainer>
            </Form>
          </Card>
          <Button variant="outlined" onClick={() => window.alert('become a qover gold member to access this feature')} >
            <span>Don't have an account?</span><UnderlinedText>Ask for access</UnderlinedText>
          </Button>
        </LoginFormContainer>
      </Background>
    </>
  )
}

const FormErrorContainer = styled.div`
  padding-top: 1rem;
  justify-content: center;
  display: flex;
`
const UnderlinedText = styled.span`
  text-decoration: underline;
  margin-left: 0.5rem;
`

const CheckBoxText = styled.span`
  font-size: 1.2rem;
  color: ${Colors.blueGrey};
`

const BottomControlsContainer = styled.div`
  flex-direction: row;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const Title = styled.h1`
  color: ${Colors.blueGrey};
`

const LoginFormContainer = styled.div`
  flex-direction: column;
  display: flex;
`

const Form = styled.form`
  flex-direction: column;
  display: flex;
`

export default LoginScreen
