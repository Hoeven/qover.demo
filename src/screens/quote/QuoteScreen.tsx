import React, { useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Background, Card } from '../../components'
import { Sizes } from '../../constants'
import TextField from '@material-ui/core/TextField'
import { InputLabel } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select';
import styled from 'styled-components';
import InputAdornment from '@material-ui/core/InputAdornment';
import { useHistory } from "react-router-dom";
import QuoteService from '../../services/quoteService'


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 400,
      },
      '& .MuiButton-root': {
        width: 150
      }
    },
    formControl: {
      margin: theme.spacing(1),
      width: 400,
      textAlign: 'left'
    }
  }),
);


const priceValidator = (price: number) => {
  if (price === 0 || price === undefined) return { error: true, message: 'Price is a required field.' }
  return price < 5000
    ? { error: true, message: 'The price of that car is too low' }
    : { error: false, message: '' }
}

const ageValidator = (age: number) => {
  if (age === 0 || age === undefined) return { error: true, message: 'Age is a required field.' }
  return age < 18
    ? { error: true, message: 'You must be over 18 years old.' }
    : { error: false, message: '' }
}

const useInput = (initialValue: any, validator?: any) => {
  const [value, setValue] = useState(initialValue);
  const [validationResult, setValidationResult] = useState({ error: false, message: '' })

  return {
    validate: () => {
      if (!validator) return true
      const result = validator(value)
      setValidationResult(result)
      return !result.error
    },
    value,
    setValue,
    reset: () => setValue(''),
    bind: {
      error: validationResult.error,
      helperText: validationResult.message,
      value,
      onChange: (event: any) => {
        setValue(event.target.value)
        setValidationResult({ error: false, message: '' })
      }
    }
  };
};


const useSelectInput = (initialValue: any) => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(''),
    bind: {
      value,
      onChange: (event: any) => {
        setValue(event.target.value)
      }
    }
  };
};

const QuoteScreen = () => {
  const classes = useStyles();
  const history = useHistory();
  const [error, setError] = useState({ error: false, errorMessage: '' });


  const { value: age, bind: bindAge, validate: validateAge } = useInput('', ageValidator);
  const { value: car, bind: bindCar } = useSelectInput('Audi');
  const { value: price, bind: bindPrice, validate: validatePrice } = useInput('', priceValidator)

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      const isAgeValid = validateAge()
      const isPriceValid = validatePrice()
      if (isAgeValid && isPriceValid) {
        const result = await QuoteService.getQuote(age, car, price)
        history.push({
          pathname: '/plan',
          state: { ...result }
        })
      }
    }
    catch (e) {
      setError({ error: true, errorMessage: e.response.data })
    }
  }

  return (
    <Background>
      <Card size={Sizes.large}>
        <form className={classes.root} noValidate autoComplete="off" onSubmit={handleSubmit}>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <StyledInputLabel>Age of the driver</StyledInputLabel>
            <TextField label="Age" type="number" variant="outlined" {...bindAge} />
          </div>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <StyledInputLabel>Type of car</StyledInputLabel>
            <FormControl className={classes.formControl}>
              <Select
                variant="outlined"
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                {...bindCar}
              >
                <MenuItem value={'Audi'}>Audi</MenuItem>
                <MenuItem value={'BMW'}>BMW</MenuItem>
                <MenuItem value={'Porsche'}>Porsche</MenuItem>
              </Select>
            </FormControl>
          </div>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <StyledInputLabel>Purchase price</StyledInputLabel>
            <TextField label="Price" type="number" variant="outlined" {...bindPrice} InputProps={{ endAdornment: <InputAdornment position="end">€</InputAdornment> }} />
          </div>
          <div style={{ display: 'flex', marginTop: '1rem', justifyContent: 'flex-end', marginRight: '5rem' }}>
            <Button variant="contained" color="secondary" type="submit">
              Get a price
          </Button>
          </div>
          {!!error.error && <FormHelperText error>{error.errorMessage}</FormHelperText>}
        </form>
      </Card>
    </Background >
  )
}

const StyledInputLabel = styled(InputLabel)`
  min-width: 200px;
  text-align: left;
`
export default QuoteScreen
