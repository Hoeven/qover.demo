import axios from 'axios'

const apiUrl = 'http://localhost:4000/api'

export const login = async (email: string, password: string): Promise<any> => {
    const response = await axios.post(`${apiUrl}/users/login`, { email, password })
    return response.data
}

export default { 
    login
}